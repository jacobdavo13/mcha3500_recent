%% RunKF
clear
clc
%% Load data
load('imuData_moving1');
% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);
% TODO: Correct angle offset
pot_angle_init = mean(potAngle(1:200));
acc_angle_init = mean(accAngle(1:200));
potAngle = potAngle - (pot_angle_init - acc_angle_init);

%% Optimisation
% Define cost
get_angle = @(x) x(:,2);
get_gyro = @(x) x(:,1) + x(:,3);
cost = @(params) norm(potAngle - get_angle(runKF(accAngle, gyroVelocity, N, params))) ...
+ norm(gyroVelocity - get_gyro(runKF(accAngle, gyroVelocity, N, params)));
% Run optimisation
params0 = [1e-4; 0; 1e-4; 0; 0; 1e-4];
params_opt = fminunc(cost,params0);
L_opt = [params_opt(1) 0 0; params_opt(2) params_opt(3) 0; params_opt(4) params_opt(5) params_opt(6)];
Q_opt = L_opt&L_opt';
                     

%% Run and Plot
[x_KF, P_KF] = runKF(accAngle, gyroVelocity,N,params_opt);

figure(1)
subplot(2,1,1)
plot(time, potAngle, time, x_KF(:,2), time, accAngle)
hold on
grid on
legend('Potentiometer angle', 'Kalman filter angle', 'Accelerometer angle', 'Location', 'southwest')

subplot(2,1,2)
plot(time, gyroVelocity, time, x_KF(:,1))
hold on
grid on
legend('Gyro velocity', 'Kalman filter velocity')


%% Load data
load('imuData_moving2');
% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);
% TODO: Correct angle offset
pot_angle_init = mean(potAngle(1:200));
acc_angle_init = mean(accAngle(1:200));
potAngle = potAngle - (pot_angle_init - acc_angle_init);

%% Optimisation
% Define cost
get_angle = @(x) x(:,2);
get_gyro = @(x) x(:,1) + x(:,3);
cost = @(params) norm(potAngle - get_angle(runKF(accAngle, gyroVelocity, N, params))) ...
+ norm(gyroVelocity - get_gyro(runKF(accAngle, gyroVelocity, N, params)));
% Run optimisation
params0 = [1e-4; 0; 1e-4; 0; 0; 1e-4];
params_opt = fminunc(cost,params0);
L_opt = [params_opt(1) 0 0; params_opt(2) params_opt(3) 0; params_opt(4) params_opt(5) params_opt(6)];
Q_opt = L_opt&L_opt';




%% Run and Plot
[x_KF, P_KF] = runKF(accAngle, gyroVelocity,N,params_opt);

figure(2)
subplot(2,1,1)
plot(time, potAngle, time, x_KF(:,2), time, accAngle)
hold on
grid on
legend('Potentiometer angle', 'Kalman filter angle', 'Accelerometer angle', 'Location', 'southwest')

subplot(2,1,2)
plot(time, gyroVelocity, time, x_KF(:,1))
hold on
grid on
legend('Gyro velocity', 'Kalman filter velocity')


%% Load data
load('imuData_still');
% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);
% TODO: Correct angle offset
pot_angle_init = mean(potAngle(1:200));
acc_angle_init = mean(accAngle(1:200));
potAngle = potAngle - (pot_angle_init - acc_angle_init);

%% Optimisation
% Define cost
get_angle = @(x) x(:,2);
get_gyro = @(x) x(:,1) + x(:,3);
cost = @(params) norm(potAngle - get_angle(runKF(accAngle, gyroVelocity, N, params))) ...
+ norm(gyroVelocity - get_gyro(runKF(accAngle, gyroVelocity, N, params)));
% Run optimisation
params0 = [1e-4; 0; 1e-4; 0; 0; 1e-4];
params_opt = fminunc(cost,params0);
L_opt = [params_opt(1) 0 0; params_opt(2) params_opt(3) 0; params_opt(4) params_opt(5) params_opt(6)];
Q_opt = L_opt&L_opt';




%% Run and Plot
[x_KF, P_KF] = runKF(accAngle, gyroVelocity,N,params_opt);

figure(3)
subplot(2,1,1)
plot(time, potAngle, time, x_KF(:,2), time, accAngle)
hold on
grid on
legend('Potentiometer angle', 'Kalman filter angle', 'Accelerometer angle', 'Location', 'southwest')

subplot(2,1,2)
plot(time, gyroVelocity, time, x_KF(:,1))
hold on
grid on
legend('Gyro velocity', 'Kalman filter velocity')


%% Functions
function [x_KF, P_KF] = runKF(angle_accel, velocity_gyro, N, params)
% TODO: Define contimuout time model
L = [params(1) 0 0; params(2) params(3) 0; params(4) params(5) params(6)];
% TODO: Compute process noise Q from L
Q = L*L';

Ac = [0 0 0; 1 0 0; 0 0 0];
Bc = [];
C = [0 1 0; 1 0 1];
% TODO: Discretise model
T = 0.005
[Ad, Bd] = c2d(Ac,Bc,T);
% Define Kalman filter initial conditions
% Initial state estimate
xm = [0; 0; 0];
% Initial estimate error covariance
Pm = 1*eye(3);
% TODO: Measurement noise covariance
R = [0.0502 0; 0 3.4546e-06];
% TODO: Process noise covariance
%Q = [1e-6 0 0; 0 1e-6 0; 0 0 1e-6];
% Allocate space to save results
x_KF = zeros(N,3);
P_KF = zeros(3,3,N);
% Run Kalman Filter


for i=1:N
% TODO: Pack measurement vector
yi = [angle_accel(i);velocity_gyro(i)];
% Correction step
% TODO: Compute Kalman gain
Kk = Pm*C.'/(C*Pm*C.' + R);
% TODO: compute corrected state estimate
xp = xm + Kk*(yi - C*xm);
% TODO: Compute new measurement error covariance
Pp = (eye(3) - Kk*C)*Pm*(eye(3) - Kk*C).' + Kk*R*Kk.';
% Prediction step
% TODO: Predict next state
xm = Ad*xp;
% TODO: Compute prediction error covariance
Pm = Ad*Pp*Ad.' + Q;
% Store results
x_KF(i,:) = xp.';
P_KF(:,:,i) = Pm;
end
end
