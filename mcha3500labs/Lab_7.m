%% Lab 7

clear
clc
% remove any persistant serial connections
if ~isempty(instrfind('Status', 'open'))
fclose(instrfind);
end
% Create connections to each device
stm_device = serial('COM3', 'BaudRate', 115200, 'Timeout', 0.5, 'Terminator', 'LF');
fopen(stm_device);
% Call the potentiometer data-logging code. 'compose' appends the correct
% new-line characters to the string
txStr = compose("logIMU");
fprintf(stm_device, txStr);


% Define expected number of samples
N = 1000;
% Initialise data structure for data
imuData.time = zeros(N,1);
imuData.angle = zeros(N,1);
imuData.velocity = zeros(N,1);
imuData.voltage = zeros(N,1);
% Collect the expected number of samples
for i=1:N
% Read in data from serial connection
rxStr = fgets(stm_device);
% Separate string into parts
separatedString = split(rxStr,',');
% Convert data to numbers and store in data structure
imuData.time(i) = str2double(separatedString{1});
imuData.angle(i) = str2double(separatedString{2});
imuData.velocity(i) = str2double(separatedString{3});
imuData.voltage(i) = str2double(separatedString{4});
end
% Plot results
figure(1)
subplot(3,1,1)
plot(imuData.time, imuData.angle, '+')
grid on
subplot(3,1,2)
plot(imuData.time, imuData.velocity, '+')
grid on
subplot(3,1,3)
plot(imuData.time, imuData.voltage, '+')
grid on

clear
clc
%% Load data
load('imuData_still');
% Unpack data
time = imuData.time;
accAngle = imuData.angle;

gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);
%% Compute sample mean and variance
% TODO: Compute sample mean
% TODO: Compute sample variance. Use Bessel's correction
mean_gyro = mean(gyroVelocity)

var_gyro_ = var(gyroVelocity)


%% Load data
load('imuData_moving1');
% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);
% TODO: Correct angle offset
pot_angle_init = mean(potAngle(1:200));
acc_angle_init = mean(accAngle(1:200));
potAngle = potAngle - (pot_angle_init - acc_angle_init);
figure(2)
plot(time,accAngle,time,potAngle)

% TODO: Compute the angle measurement 'noise'
angle_noise = accAngle - potAngle;
%% Compute sample mean and covariance
% TODO: Compute sample mean
% TODO: Compute sample covariance. Use Bessel's correction
mean_noise = mean(angle_noise)

var_noise = var(angle_noise)

