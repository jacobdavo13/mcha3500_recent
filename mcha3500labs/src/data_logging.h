#ifndef DATA_LOGGING_H
#define DATA_LOGGING_H
#include <stdint.h>
void logging_init(void);
void pend_logging_start(void);
void logging_stop(void);
static void log_IMU(void);
void IMU_logging_start(void);
#endif