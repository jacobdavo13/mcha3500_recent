#include "data_logging.h"
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
#include "IMU.h"
uint16_t logCount;


#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

static void (*log_function)(void);
static void log_pointer(void *argument);
 
static osTimerId_t Logging_TimerID;
static osTimerAttr_t Logging_TimerAttr = 
{
	.name = "Logging_Timer",
};

static void pend_log(void);


static void pend_log(void)
{
	float voltagelog = pendulum_read_voltage();
	printf("%f,%f\n", logCount*0.001, voltagelog);
	logCount = logCount + 5;
	if(logCount == 2000)
	{
		logging_stop();
	}	
}

 void logging_init(void)
{
	Logging_TimerID = osTimerNew(log_pointer, osTimerPeriodic, NULL, &Logging_TimerAttr);
/* TODO: Initialise timer for use with pendulum data logging */
}

static void log_pointer(void *argument)
{
	UNUSED(argument);
	
	(*log_function)();
}

void pend_logging_start(void)
{
	log_function = &pend_log;
	if(!osTimerIsRunning(Logging_TimerID))
	{
	logCount = 0;
	osTimerStart(Logging_TimerID, 5);
	}
}

void logging_stop(void)
{
	if(osTimerIsRunning(Logging_TimerID))
	{
	osTimerStop(Logging_TimerID);
	}
/* TODO: Stop data logging timer */
}

void IMU_logging_start(void)
{
	log_function = &log_IMU;
	if(!osTimerIsRunning(Logging_TimerID))
	{
	logCount = 0;
	osTimerStart(Logging_TimerID, 5);
	}
	
}


static void log_IMU(void)
{
	IMU_read();
	float IMU_angle_fin = get_acc_angle();
	float gyro_X_fin = get_gyroX();
	float voltagelog = pendulum_read_voltage();
	printf("%f,%f,%f,%f\n", logCount*0.001,IMU_angle_fin*(180.0/M_PI),gyro_X_fin*(180.0/M_PI),voltagelog);
	logCount = logCount + 5;
	if(logCount == 5000)
	{
		logging_stop();
	}		
}

