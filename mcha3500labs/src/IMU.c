#include "tm_stm32_mpu6050.h"
#include "math.h"
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "IMU.h"
/* Variable declarations */
TM_MPU6050_t IMU_datastruct;

static float acc_Y;
static float acc_Z;
static float gyro_X;
static float IMU_angle;

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif


/* Function defintions */
void IMU_init(void)
{
	TM_MPU6050_Init(&IMU_datastruct,TM_MPU6050_Device_0,TM_MPU6050_Accelerometer_4G,TM_MPU6050_Gyroscope_250s);
	/* TODO: Initialise IMU with AD0 LOW, accelleration sensitivity +-4g, gyroscope +-250 deg/s */
}

void IMU_read(void)
{
	TM_MPU6050_ReadAll(&IMU_datastruct);
}

float get_accY(void)
{
	acc_Y = IMU_datastruct.Accelerometer_Y*(4.0*9.81/32787.0);
	return acc_Y;
}

float get_accZ(void)
{
	acc_Z = IMU_datastruct.Accelerometer_Z*(4.0*9.81/32787.0);
	return acc_Z;
}

float get_gyroX(void)
{
	gyro_X = IMU_datastruct.Gyroscope_X*(250/32787.0)*(M_PI/180.0);
	return gyro_X;
}

double get_acc_angle(void)
{
	IMU_angle = -atan2(get_accZ(),get_accY());
	return IMU_angle;
}