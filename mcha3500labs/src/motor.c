#include "motor.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include <stdint.h>

int32_t enc_count = 0;
static GPIO_InitTypeDef GPIO_InitStructureA;
static GPIO_InitTypeDef GPIO_InitStructureC;
static TIM_HandleTypeDef htim3;
static TIM_OC_InitTypeDef sConfigPWM_motor;


 
 void motor_PWM_init(void)
 {
		 
		__HAL_RCC_GPIOA_CLK_ENABLE();
		__HAL_RCC_TIM3_CLK_ENABLE();
 
         GPIO_InitStructureA.Pin = GPIO_PIN_6;
        GPIO_InitStructureA.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStructureA.Pull = GPIO_NOPULL;
        GPIO_InitStructureA.Speed = GPIO_SPEED_FREQ_HIGH;
		GPIO_InitStructureA.Alternate = GPIO_AF2_TIM3;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStructureA);
		
		htim3.Instance = TIM3;
		htim3.Init.Prescaler = 1;
		htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
		htim3.Init.Period = 10000;
		htim3.Init.ClockDivision = 0; 
		
		sConfigPWM_motor.OCMode = TIM_OCMODE_PWM1;
		sConfigPWM_motor.Pulse = 0;
		sConfigPWM_motor.OCPolarity = TIM_OCPOLARITY_HIGH;
		sConfigPWM_motor.OCFastMode = TIM_OCFAST_DISABLE;
		
		HAL_TIM_PWM_Init(&htim3);
		HAL_TIM_PWM_ConfigChannel(&htim3,&sConfigPWM_motor,TIM_CHANNEL_1);
		
		
		
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (10000/100)*25);
		
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
 }
 
 
 
 
 void motor_encoder_init(void)
{
 /* TODO: Enable GPIOC clock */
 /* TODO: Initialise PC0, PC1 with:
 - Pin 0|1
 - Interrupt rising and falling edge
 - No pull
 - High frequency */
		__HAL_RCC_GPIOC_CLK_ENABLE();
		GPIO_InitStructureC.Pin = GPIO_PIN_0|GPIO_PIN_1;
		GPIO_InitStructureC.Mode = GPIO_MODE_IT_RISING_FALLING;
		GPIO_InitStructureC.Pull = GPIO_NOPULL;
		GPIO_InitStructureC.Speed = GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStructureC);
		
		HAL_NVIC_SetPriority(EXTI0_IRQn,0x0f,0x0f);
		HAL_NVIC_SetPriority(EXTI1_IRQn,0x0f,0x0f);
		
		HAL_NVIC_EnableIRQ(EXTI0_IRQn);
		HAL_NVIC_EnableIRQ(EXTI1_IRQn);
}

void EXTI0_IRQHandler(void)
{
	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0)== HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1))
	{
		enc_count = enc_count + 1;
	}
	else
	{
		enc_count = enc_count - 1;
	}
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

void EXTI1_IRQHandler(void)
{
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0)== HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1))
	{
		enc_count = enc_count - 1;
	}
	else
	{
		enc_count = enc_count + 1;
	}
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

int32_t motor_encoder_getValue(void)
{
return enc_count;
}